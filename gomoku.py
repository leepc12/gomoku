#!/usr/bin/env python2

import re, math, time
import collections

verbose = True

class Gomoku(object):
    '''    
    * game name: Gomoku, Renju, Omok or five in a row

    * Pro rule:
        N = 15 by default (must be odd)
        first stone: black goes first and must put a stone in the center
        second stone: no limit
        third stone: black cannot put a stone in 5x5 around the center

    * state: series of action ((i,j),(i,j),...)
    * action: position of stone (i,j)
    * player: player to place a next stone from a state, +1=black(O), -1=white(X)
    * secondStonePos: due to the symmetry, second stones must be on either 1 or 2
    == Board =========================
       | a b c d e f g h i j k l m n o
    ---+------------------------------
    15 | . . . . . . . . . . . . . . .
    14 | . . . . . . . . . . . . . . .
    13 | . . . . . . . . . . . . . . .
    12 | . . . . . . . . . . . . . . .
    11 | . . . . . . . . . . O . . . .
    10 | . . . . . . . . . . . . . . .
    09 | . . . . . . . 1 2 . . . . . .
    08 | . . . . . . . O . . . . . . .
    07 | . . . . . . . . . . . . . . .
    06 | . . . . . . . . . . . . . . .
    05 | . . . . . . . . . . . . . . .
    04 | . . . . . . . . . . . . . . .
    03 | . . . . . . . . . . . . . . .
    02 | . . . . . . . . . . . . . . .
    01 | . . . . . . . . . . . . . . .
    '''
    def __init__(self, N=15, secondStonePos=2):
        if N>26:
            raise ValueError('N must be <= 26!')
        if N<5:
            raise ValueError('N must be >= 5!')
        if N%2==0:
            raise ValueError('N must odd!')
        self.N = N
        self.cacheBoard = dict()
        self.BIG_NUMBER = 0 #1000000 # to prevent negative V
        self.secondStonePos = secondStonePos

    def resetCache(self):
        if verbose: print 'Resetting cache... (# states in cache:)', len(self.cacheBoard)
        del self.cacheBoard
        self.cacheBoard = dict()

    def startState(self):
        return ((self.N/2,self.N/2),)

    def player(self, state): # player: +1=black(O), -1=white(X)
        return 1 if len(state)%2==0 else -1

    def isEnd(self, state):
        # board, _, numThrees, numFours, numFives = self.getBoard(state)
        # result = False
        # for p in [-1,1]:
        #     result += numFives[p]>0 # five stones in a row
        #     result += numFours[(p,0)] # any open four stones in a row
        #     result += numFours[(p,1)]>1 # two closed4's
        #     result += (numFours[(p,1)]>0 and numThrees[(p,0)]>0) # closed4+open3
        # return result
        board, forks, numSeries, numForks = self.getBoard(state)

        p = -self.player(state)
        
        result = False
        result += (numSeries[(p,5,False)] + numSeries[(p,5,True)]) # five stones in a row
        if not (numSeries[(-p,4,False)] or numSeries[(-p,4,True)]):
            result += (numSeries[(p,4,False)] + numSeries[(p,4,True)]>1 + numForks[(p,3,4,False,True)])
        return result
    def isLosing(self, state):
        # board, _, numThrees, numFours, numFives = self.getBoard(state)
        # result = False
        # for p in [-1,1]:
        #     result += numFives[p]>0 # five stones in a row
        #     result += numFours[(p,0)] # any open four stones in a row
        #     result += numFours[(p,1)]>1 # two closed4's
        #     result += (numFours[(p,1)]>0 and numThrees[(p,0)]>0) # closed4+open3
        # return result
        if self.isEnd(state): return False
        board, forks, numSeries, numForks = self.getBoard(state)

        p = -self.player(state)
        result = False
        if not ( numSeries[(p,4,False)] or numSeries[(p,4,True)] ):
            result += numSeries[(-p,3,False)]
        return result
    
    def actions(self, state, adj=1): # position of stone: (i,j)
        # board, _, _, _, _ = self.getBoard(state)        
        board, _, _, _ = self.getBoard(state)
        result = []
        if len(state) == 1:
            if self.secondStonePos==1:
                result = [(self.N/2+1,self.N/2+1)]
            elif self.secondStonePos==2:
                result = [(self.N/2,self.N/2+1)]
            else:
                raise ValueError
        elif len(state) == 2:
            # Rule Pro
            # rangeI = range(self.N/2-3,self.N/2-3)+range(self.N/2+2,self.N/2+3)
            rangeI = range(self.N/2-3,self.N/2+4)
            rangeJ = rangeI
            for i in rangeI:
                for j in rangeJ:
                    if board[i][j] == 0 and \
                        (i==self.N/2-3 or i==self.N/2+3 or j==self.N/2-3 or j==self.N/2+3):
                            result.append((i,j))
            # # Rule Standard
            # rangeI = range(self.N/2-1,self.N/2+2)
            # rangeJ = rangeI
            # for i in rangeI:
            #     for j in rangeJ:
            #         if board[i][j]==0:
            #             nearByStoneFound = False
            #             for ii in range( max(0,i-adj), min(i+adj,self.N-1)+1):
            #                 for jj in range( max(0,j-adj), min(j+adj,self.N-1)+1):
            #                     if board[ii][jj]!=0:
            #                         nearByStoneFound = True
            #                         break;
            #                 if nearByStoneFound:
            #                     break;
            #             if nearByStoneFound:
            #                 result.append((i,j))
        else:
            for turn, pos in enumerate(state):
                i, j = pos
                for ii in range( max(0,i-adj), min(i+adj,self.N-1)+1):
                    for jj in range( max(0,j-adj), min(j+adj,self.N-1)+1):
                        if (ii != i or jj != j) and board[ii][jj] == 0 and not (ii,jj) in result:
                            result.append((ii,jj))
        # result2 = []
        # for action in result:
        #     succState = self.succ(state, action)
        #     if not self.isLosing(succState):
        #         result2.append(action)
        result2 = result

        return result2

    def succ(self, state, action):
        return state + (action,)

    def reward(self, state, player):
        # reward = (         2*numTwos[(p,0)]   +       numTwos[(p,1)]   \
        #            +     200*numThrees[(p,0)] +     2*numThrees[(p,1)] \
        #            +  200000*numFours[(p,0)]  +   200*numFours[(p,1)] \
        #            +  200000*numFives[p] ) \
        #         -(         2*numTwos[(-p,0)]  +       numTwos[(-p,1)]   \
        #            +    2000*numThrees[(-p,0)]+    20*numThrees[(-p,1)] \
        #            +  100000*numFours[(-p,0)] +  2000*numFours[(-p,1)] \
        #            +  100000*numFives[-p] )
        board, forks, numSeries, numForks = self.getBoard(state)
        p = player
        reward = 0
        # score for series
        # reward += (        2*numSeries[(p,2,False)]  +      numSeries[(p,2,True)] \
        #              +     8*numSeries[(p,3,False)]  +    2*numSeries[(p,3,True)] \
        #              +    16*numSeries[(p,4,False)]  +    8*numSeries[(p,4,True)] \
        #              +   320*numSeries[(p,5,False)]  +   16*numSeries[(p,5,True)])
        # reward -=0.5*(   2*numSeries[(-p,2,False)]  +      numSeries[(-p,2,True)] \
        #              +     8*numSeries[(-p,3,False)] +    2*numSeries[(-p,3,True)] \
        #              +    16*numSeries[(-p,4,False)] +    8*numSeries[(-p,4,True)] \
        #              +   320*numSeries[(-p,5,False)] +  160*numSeries[(-p,5,True)])
        # # penalty for threat
        # reward -=1.5*(   100*numSeries[(-p,3,False)]  \
        #               +  200*numSeries[(-p,4,False)] +  100*numSeries[(-p,4,True)] \
        #               +  400*numSeries[(-p,5,False)] +  200*numSeries[(-p,5,True)] )

        # reward += (        2*numSeries[(p,2,False)]  +      numSeries[(p,2,True)] \
        #              +    32*numSeries[(p,3,False)]  +    2*numSeries[(p,3,True)] \
        #              +   128*numSeries[(p,4,False)]  +   32*numSeries[(p,4,True)] \
        #              +   256*numSeries[(p,5,False)]  +  256*numSeries[(p,5,True)])
        # reward -=5.0*(     2*numSeries[(-p,2,False)] +      numSeries[(-p,2,True)] \
        #              +   128*numSeries[(-p,3,False)] +    2*numSeries[(-p,3,True)] \
        #              +   128*numSeries[(-p,4,False)] +  128*numSeries[(-p,4,True)] \
        #              +   256*numSeries[(-p,5,False)] +  128*numSeries[(-p,5,True)])
        # reward -=1.5*(   200*numForks[(-p,3,4,False,True)] + 200*numForks[(-p,3,4,False,False)] \
        #               +  200*numForks[(-p,4,4,True,True)] + 200*numForks[(-p,4,4,True,False)] \
        #               +  200*numForks[(-p,4,4,False,True)]) \

        # reward += (        2*numSeries[(p,2,False)]  +       numSeries[(p,2,True)] \
        #              +   200*numSeries[(p,3,False)]  +     2*numSeries[(p,3,True)] \
        #              +200000*numSeries[(p,4,False)]  +   200*numSeries[(p,4,True)] \
        #              +200000*numSeries[(p,5,False)]  +200000*numSeries[(p,5,True)])
        # reward -=1.0*(     2*numSeries[(-p,2,False)] +       numSeries[(-p,2,True)] \
        #              +  2000*numSeries[(-p,3,False)] +    20*numSeries[(-p,3,True)] \
        #              +100000*numSeries[(-p,4,False)] +  2000*numSeries[(-p,4,True)] \
        #              +100000*numSeries[(-p,5,False)] +100000*numSeries[(-p,5,True)])

        reward += (        2*numSeries[(p,2,False)]  +       numSeries[(p,2,True)] \
                     +   200*numSeries[(p,3,False)]  +     2*numSeries[(p,3,True)] \
                     +  2000*numSeries[(p,4,False)]  +   200*numSeries[(p,4,True)] \
                     +  2000*numSeries[(p,5,False)]  +  2000*numSeries[(p,5,True)])
        reward -=1.0*(     2*numSeries[(-p,2,False)] +       numSeries[(-p,2,True)] \
                     +  2000*numSeries[(-p,3,False)] +    20*numSeries[(-p,3,True)] \
                     + 20000*numSeries[(-p,4,False)] +  2000*numSeries[(-p,4,True)] \
                     + 20000*numSeries[(-p,5,False)] + 20000*numSeries[(-p,5,True)])
        return reward # multiply player(+1 or -1) to score to distinguish me and enemy

    def getBoard(self, state):
        if state in self.cacheBoard:
            return self.cacheBoard[state]
        # prepare board grid
        board = list()
        for i in range(self.N):
            board.append(collections.defaultdict(int))
        for turn, pos in enumerate(state):
            i, j = pos
            board[i][j] = 1 if turn%2==0 else -1
        # init (p,numStones,closed)
        numSeries = { (p,numStones,closed):0 for p in [-1,1] \
                        for numStones in range(2,6) for closed in [True,False] }
        numForks = { (p,numStones1,numStones2,closed1,closed2):0 for p in [-1,1] \
                    for numStones1 in range(2,6) for numStones2 in range(numStones1,6) \
                        for closed1 in [True,False] for closed2 in [True,False] }
        
        directions = [(1,0),(0,1),(1,1),(1,-1)]

        def add(a, b, m=1):
            return [a[i]+m*b[i] for i in range(len(a))]
        def subtract(a, b, m=1):
            return [a[i]-m*b[i] for i in range(len(a))]
        def outOfBoard(pos):
            return pos[0]<0 or pos[0]>self.N-1 or pos[1]<0 or pos[1]>self.N-1
        def getStone(pos):
            return board[pos[0]][pos[1]] if not outOfBoard(pos) else None
            
        class Fork:
            def __init__(self, pos, dir):
                self.pos = pos
                self.dir = dir
                self.valid = False
                self.seq = [pos]
                self.prevBlocked = False
                self.nextBlocked = False
                self.closed = False
                self.numStones = 1
                stone = getStone(pos)
                # check left
                for i in range(1,6):
                    prev = subtract(pos, dir, i)
                    prevStone = getStone(prev)
                    if i in [1,3] and prevStone==stone: return # invalid fork
                    if prevStone in [-stone,None]: break
                if i==1:
                    self.prevBlocked = True
                # check right
                cntEmpty = 0
                for j in range(1,6):
                    next = add(pos, dir, j)
                    nextStone = getStone(next)
                    if nextStone in [-stone,None]:                      
                        prev = add(pos, dir, j-1)
                        prevStone = getStone(prev)
                        if prevStone==stone:
                            self.nextBlocked = True
                        break
                    if nextStone==0: cntEmpty += 1
                    if cntEmpty<2:
                        if nextStone==stone: self.numStones += 1
                        self.seq.append(next)
                if j+i<=5:
                    return # invalid fork
                if self.numStones<=1:
                    return
                if j+i<=5 and self.prevBlocked and self.nextBlocked:
                    return
                self.closed = self.prevBlocked or self.nextBlocked
                if getStone(self.seq[-1])==0: self.seq.pop()
                if len(self.seq)==5 and getStone(self.seq[2])==0:
                    self.closed = True
                self.valid = True
                # if pos==(11,3) and len(self.seq)>=3:
                #     print getStone(self.seq[0]), i, j, self.seq, cntEmpty, self.prevBlocked, self.nextBlocked
            def intersectsWith(self, fork):
                for pos in self.seq:
                    for pos2 in fork.seq:
                        if pos==pos2:
                            return True
                return False

        forks = { 1:list(), -1:list() }

        parentState = state[:-1]
        if parentState in self.cacheBoard:
            _, cachedForks, _, _ = self.cacheBoard[parentState]
            x, y = state[-1]
            stateToSearchForks = []
            for player in [1,-1]:
                for fork in cachedForks[player]:
                    assert(fork.valid)
                    invalid = False
                    for pos in fork.seq:
                        if pos[0] in range(x-2,x+3) and pos[1] in range(y-2,y+3):
                            invalid = True
                            break
                    if invalid:
                        stateToSearchForks.append((fork.seq[0],[fork.dir]))
                    else:
                        # print "using cached", player, fork.seq
                        forks[player].append(fork)
            for pos in state:
                if pos[0] in range(x-2,x+3) and pos[1] in range(y-2,y+3):
                    for dir in directions:
                        if not (pos, [dir]) in stateToSearchForks:
                            stateToSearchForks.append((pos,[dir]))
        else:
            stateToSearchForks = [(pos,directions) for pos in state]

        for pos, dirs in stateToSearchForks:
            player = getStone(pos)
            for dir in dirs:
                # print pos, dir
                fork = Fork(pos,dir)
                if fork.valid and not fork in forks[player]:
                    # for f in forks[player]:
                    #     assert(f.seq!=fork.seq)
                    forks[player].append(fork)
                    # print 'appending', player, fork.seq

        for player in [1,-1]:            
            for i in range(len(forks[player])):                                
                fork1 = forks[player][i]
                if not fork1.valid: continue
                # count series
                numSeries[(player,fork1.numStones,fork1.closed)] += 1
                for j in range(i+1,len(forks[player])):
                    fork2 = forks[player][j]
                    if not fork2.valid: continue
                    # count forks
                    if fork1.intersectsWith(fork2):
                        if fork1.numStones<=fork2.numStones:                            
                            numForks[(player,fork1.numStones,fork2.numStones,fork1.closed,fork2.closed)] += 1
                        else:
                            numForks[(player,fork2.numStones,fork1.numStones,fork2.closed,fork1.closed)] += 1
        result = (board, forks, numSeries, numForks)
        self.cacheBoard[state] = result
        return result

    # for human interface, visualization
    def humanInputToAction(self, input): # humanInput (alphabet,number) to action (i,j)
        input = input.lower()
        x = re.search('[a-z]', input)
        y = re.search('\d+', input)
        if x:
            x = self.__a2i(x.group(0))
        else:
            print 'wrong x! (%s)' % (input,)
            return None
        if y and y.group(0) > 0:
            y = int(y.group(0))-1
        else:
            print 'wrong y! (%s)' % (input,)
            return None
        return (x, y)

    def actionToHumanInput(self, action):
        return self.__i2a(action[0])+str(action[1]+1)

    def stateToSeq(self, state):
        moves = ''
        for pos in state:
            moves += "%s%d-" % (self.__i2a(pos[0]),pos[1]+1)
        return moves        

    def printState(self, state):
        if not verbose: return
        # header
        print
        print "== Board ==========================="
        header1 = '      |'
        header2 = '      |'
        line = '------+'
        for i in range(self.N):
            header1 += ("%2d" % (i,) )
            header2 += (' ' + self.__i2a(i))
            line += '--'
        print header1
        print header2
        print line
        # board, numTwos, numThrees, numFours, numFives = self.getBoard(state)
        board, forks, numSeries, numForks = self.getBoard(state)
        for j in reversed(range(self.N)):
            line = "%2d %2d |" % (j, j+1)
            for i in range(self.N):
                if board[i][j]==1:
                    line += ' o'
                elif board[i][j]==-1:
                    line += (' ' + 'x')
                else:
                    line += ' .'
            print line
        print "== Moves ========================="        
        print self.stateToSeq(state)
        # print state
        # print "== Black(O) ======================"
        # print "OpenTwos, ClosedTwos:", numTwos[(1,0)], numTwos[(1,1)], \
        #       "OpenThrees, ClosedThrees:", numThrees[(1,0)], numThrees[(1,1)], \
        #       "OpenFours, ClosedFours:", numFours[(1,0)], numFours[(1,1)], \
        #       "Fives", numFives[1]
        # print "== White(X) ======================"
        # print "OpenTwos, ClosedTwos:", numTwos[(-1,0)], numTwos[(-1,1)], \
        #       "OpenThrees, ClosedThrees:", numThrees[(-1,0)], numThrees[(-1,1)], \
        #       "OpenFours, ClosedFours:", numFours[(-1,0)], numFours[(-1,1)], \
        #       "Fives", numFives[-1]
        print "== numSeries (Black/White)"
        for numStones in range(3,6):                
            for closed in [0,1]:
                s = ""
                for p in [1,-1]:
                    s+= "\t\t%d(%d): %d" % (numStones, closed, numSeries[(p,numStones,closed)])
                print s
        print " numForks (Black/White) =="
        for numStones1 in range(3,6):
            for closed1 in [0,1]:
                for numStones2 in range(numStones1+1,6):
                    for closed2 in [0,1]:
                        s = ""
                        for p in [1,-1]:
                            s += "\t%d(%d)-%d(%d): %d" % (numStones1, closed1, numStones2, closed2, \
                                numForks[(p,numStones1,numStones2,closed1,closed2)] )
                        print s

    def __i2a(self, i): # 0 -> a, 1 -> b, ...
        return chr(i+97)

    def __a2i(self, a): # a -> 0, b -> 1, ...
        return ord(a.lower())-97
