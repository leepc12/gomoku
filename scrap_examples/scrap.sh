#!/bin/bash

# WC1: http://gomokuworld.com/tournaments/97
mkdir -p wc1
cd wc1
for i in `seq 7408 7436`; do
  wget -bqc http://gomokuworld.com/tournaments/97/$i
  sleep 3
done 
cd ..

# WC2: http://gomokuworld.com/tournaments/97
mkdir wc2
cd wc2
for i in `seq 7597 7654`; do
  wget -bqc http://gomokuworld.com/tournaments/98/$i
  sleep 3
done
cd ..

# parse file
find . -type f | xargs cat | grep moves | awk 'BEGIN{FS="moves\="} {print $2}' | awk 'BEGIN{FS="\&size"} {print $1}' > examples.txt

