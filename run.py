#!/usr/bin/env python2

import gomoku
import policy
import argparse, time
import gomokuML

# import cProfile
# from guppy import hpy


parser = argparse.ArgumentParser(prog='Gomoku engine and AI v1.0 (by Jin Lee, 2016)', \
    description='Policies: '+' '.join(policy.allPolicies))
parser.add_argument('black_policy', metavar='black-policy', type=str, \
    help='Policy for a black (first) player (+1)')
parser.add_argument('white_policy', metavar='white-policy', type=str, \
    help='Policy for a white (second) player (-1)')
parser.add_argument('--depth', type=int, default=2, \
    help='Depth limit for MiniMax search')
parser.add_argument('--black-use-weights', dest='black_use_weights', action='store_true', \
    help='Use learned weights for black')
parser.add_argument('--white-use-weights', dest='white_use_weights', action='store_true', \
    help='Use learned weights for white')
parser.add_argument('--black-feature-set', type=int, default=1, \
    help='Feature set for black (1: score + numStonesInARow , 2: 1 + stones in proximity')
parser.add_argument('--white-feature-set', type=int, default=1, \
    help='Feature set for white (1: score + numStonesInARow , 2: 1 + stones in proximity')
parser.add_argument('--second-stone-pos', type=int, default=2, \
    help='Second stone\'s (white) position')
parser.add_argument('--black-learn-TD', dest='black_learn_TD', action='store_true', \
    help='Learn TD weights for black')
parser.add_argument('--black-use-learned-TD', dest='black_use_learned_TD', action='store_true', \
    help='Use TD learning for black')


args = parser.parse_args()

args.black_policy+="Policy"
args.white_policy+="Policy"

if not args.black_policy in policy.allPolicies:
    raise ValueError('Choose black policy!')
if not args.white_policy in policy.allPolicies:
    raise ValueError('Choose white policy!')

game = gomoku.Gomoku(secondStonePos=args.second_stone_pos)

policies = {+1: getattr(policy,args.black_policy), \
            -1: getattr(policy,args.white_policy) }

evalFuncs = dict()

if args.black_use_weights: # choose evaluation function

    featureChosen = gomokuML.GomokuML(game, args.black_feature_set)
    featureChosen.loadWeights('weights%d.txt'%(args.black_feature_set,)) # load learned weight according to the feature_set
    evalFuncs[1] = featureChosen.evalFunc
else:
    evalFuncs[1] = game.reward

if args.white_use_weights: # choose evaluation function
    featureChosen = gomokuML.GomokuML(game, args.white_feature_set)
    featureChosen.loadWeights('weights%d.txt'%(args.white_feature_set,)) # load learned weight according to the feature_set
    evalFuncs[-1] = featureChosen.evalFunc
else:
    evalFuncs[-1] = game.reward

if args.black_learn_TD or args.black_use_learned_TD:
    featureChosen = gomokuML.GomokuML(game, args.black_feature_set) #args.black_feature_set)
    weights = featureChosen.getFeature( game.startState(), 1 )    

if args.black_use_learned_TD:
    with open('weights_TD.txt','r') as f:
        weights = [float(w) for w in f.read().split('\n')]
    def evalFunc_TD(state, player):
        return gomokuML.dot( featureChosen.getFeature(state, player), weights )
    evalFuncs[1] = evalFunc_TD

if args.black_learn_TD:
    policies[1] = getattr(policy,'randomPolicy')
    gomoku.verbose = False

def run():
    state = game.startState()
    cnt=0
    while not game.isEnd(state):
        cnt+=1
        game.printState(state)
        player = game.player(state)
        policy = policies[player]
        if gomoku.verbose:
            print '== Info =========================='
            print 'search breadth:', len(game.actions(state))
            print 'turn:', 'black' if player==1 else 'white'
        start = time.time()
        action = policy(game, state, evalFuncs[player], args.depth)
        if args.black_learn_TD and player==1:
            succState = game.succ(state, action)
            reward = evalFuncs[1](succState, player)
            
            phiS = featureChosen.getFeature(state,player)
            phiS_ = featureChosen.getFeature(succState,player)
            prediction = gomokuML.dot( phiS, weights )
            target = reward + 0.2*gomokuML.dot( phiS_, weights )
            for i in range(len(weights)):
                weights[i] -= 0.01*(prediction-target)*phiS[i]            

        if gomoku.verbose:
            print "comp. time:", time.time()-start
        if cnt%10==0: game.resetCache()
        # print hpy().heap() # debugging memory leak
        state = game.succ(state, action)
    game.printState(state)
    print "\n== Finished! ====================="
    print 'Winner:', 'Black(O)' if -game.player(state)==1 else 'White(X)'

# cProfile.runctx('run()', None, locals())
if not args.black_learn_TD:
    run()
else:
    for i in range(10000):
        run()
        print i
    print weights
    with open('weights_TD.txt','w') as f:
        f.write('\n'.join([str(w) for w in weights]))

# winning example: humanPolicy vs greedyPolicy
'''
== Board =========================
   | a b c d e f g h i j k l m n o
---+------------------------------
15 | . . . . . . . . . . . . . . .
14 | . . . . . . . . . . . . . . .
13 | . . . . . . . . . . . . . . .
12 | . . . . . . . . . . . . . . .
11 | . . . . X X O . . . . . . . .
10 | . . . . . O O X . . . . . . .
09 | . . . X O O O O X X . . . . .
08 | . . . X . X X O O . . . . . .
07 | . . . . . X X O . O . . . . .
06 | . . . . . X O O . . O . . . .
05 | . . . . . O . X . . . . . . .
04 | . . . . X . . . . . . . . . .
03 | . . . . . . . . . . . . . . .
02 | . . . . . . . . . . . . . . .
01 | . . . . . . . . . . . . . . .
== Moves =========================
h8-g7-g9-f7-h7-f8-h6-h5-g6-f6-f5-e4-h9-h10-f9-i9-f10-e11-e9-d9-g11-d8-g10-g8-i8-j9-j7-f11-k6-
== Black(O) ======================
OpenThrees, ClosedThrees: 0 3 OpenFours, ClosedFours: 0 0 Fives 1
== White(X) ======================
OpenThrees, ClosedThrees: 0 0 OpenFours, ClosedFours: 0 0 Fives 0
== Finished! =====================
Winner: Black(O)
'''