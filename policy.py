#!/usr/bin/env python2

import time, random

allPolicies = ['humanPolicy', 'greedyPolicy', 'minimaxPolicy', 'alphaBetaMinimaxPolicy', 'randomPolicy']

random.seed(1221)

def humanPolicy(game, state, evalFunc=None, depth_limit=0):
    while True:
        print 'Enter position (xy) e.g. a10:'
        input = raw_input().strip()
        action = game.humanInputToAction(input)
        if action in game.actions(state):
            break
    return action

def greedyPolicy(game, state, evalFunc, depth_limit=0):
    # print "computing... (greedy)"
    actions = game.actions(state)
    player = game.player(state)
    result = []
    for action in actions:
        succState = game.succ(state, action)
        reward = evalFunc(succState, player)
        # print player, action, reward
        result.append((reward, action))
    return max( result )[1]

def randomPolicy(game, state, evalFunc, depth_limit=0):
    # print "computing... (Monte Carlo for TD learning)"
    actions = game.actions(state)    

    winningActions = []
    for action in actions:
        succState = game.succ(state, action)
        if not game.isLosing(succState):
            winningActions.append(action)
    if winningActions:
        return winningActions[random.randrange(len(winningActions))]
    else:
        return actions[random.randrange(len(actions))]

def minimaxPolicy(game, state, evalFunc, depth_limit):
    player = game.player(state)
    def recurse(state, depth): # V
        if game.isEnd(state):
            return evalFunc(state, player)
        if depth==0:
            return evalFunc(state, player)
        if player == game.player(state):
            choices = [(recurse(game.succ(state, action), depth-1), action) \
                        for action in game.actions(state)]
            if depth == depth_limit:
                return max( choices )
            else:
                return max( choices )[0]
        else:
            choices = [recurse(game.succ(state, action), depth-1) \
                        for action in game.actions(state)]
            return min( choices )

    result = recurse(state, depth_limit)[1]
    return result

def alphaBetaMinimaxPolicy(game, state, evalFunc, depth_limit):
    player = game.player(state)
    def recurse(state, depth, alpha, beta): # V
        if game.isEnd(state):
            return evalFunc(state, player)
        if depth==0:
            return evalFunc(state, player)
        if player == game.player(state):
            a = (None, None)
            for action in game.actions(state):
                V = recurse(game.succ(state, action), depth-1, a[0], None)
                if not a[0] or V >= a[0]:
                    a = (V, action)
                if beta and V > beta:
                    break;
            if depth==depth_limit:
                return a
            else:
                return a[0]
        else:
            b = (None, None)
            for action in game.actions(state):
                V = recurse(game.succ(state, action), depth-1, None, b[0])
                if not b[0] or V <= b[0]:
                    b = (V, action)
                if alpha and V < alpha:
                    break
            return b[0]
    result = recurse(state, depth_limit, None, None)[1]
    return result

def alphaBetaMinimaxPolicy(game, state, evalFunc, depth_limit):
    player = game.player(state)
    def recurse(state, depth, alpha, beta): # V
        if game.isEnd(state):
            return evalFunc(state, player)
        if depth==0:
            return evalFunc(state, player)
        if player == game.player(state):
            a = (None, None)
            for action in game.actions(state):
                V = recurse(game.succ(state, action), depth-1, a[0], None)
                if not a[0] or V >= a[0]:
                    a = (V, action)
                if beta and V > beta:
                    break;
            if depth==depth_limit:
                return a
            else:
                return a[0]
        else:
            b = (None, None)
            for action in game.actions(state):
                V = recurse(game.succ(state, action), depth-1, None, b[0])
                if not b[0] or V <= b[0]:
                    b = (V, action)
                if alpha and V < alpha:
                    break
            return b[0]
    result = recurse(state, depth_limit, None, None)[1]
    return result
