import time
from numpy import arange,array,linalg,vstack

# * linear regression for evalFunc in Gomoku game
#  x = feature set
#  y = sum of scores (discounted) aggregated to the end
# score(state):
#     numTwos(state), ..., numFives(state)
# basic features:
#     numTwos(state), ..., numFives(state)
# more features:
#     numStones (b,w) in proximity (1, 2, 3)



class GomokuML:
    def __init__(self, game, featureSet, discount=0.2): # 0.2
        self.game = game
        self.featureSet = featureSet
        self.discount = discount
        # self.eta = eta
        self.weights = [] # will be initialized in learnWeights()

    def getFeature(self, state, player):
        p = player
        # board, numTwos, numThrees, numFours, numFives = self.game.getBoard(state)
        board, forks, numSeries, numForks = self.game.getBoard(state)
        score = self.game.reward(state, p)
        # feature1 = [1,score,\
        #     numTwos[(p,0)],numTwos[(p,1)],numTwos[(-p,0)],numTwos[(-p,1)],\
        #     numThrees[(p,0)],numThrees[(p,1)],numThrees[(-p,0)],numThrees[(-p,1)],\
        #     numFours[(p,0)],numFours[(p,1)],numFours[(-p,0)],numFours[(-p,1)],\
        #     numFives[p],numFives[-p],\
        # ]
        feature1 = [1, score/10000]
        feature1 += [ numSeries[(pp,numStones,closed)] for pp in [p,-p] \
                        for numStones in range(2,6) for closed in [True,False] ]
        if self.featureSet == 1:
            return feature1

        feature2 = [ numForks[(pp,numStones1,numStones2,closed1,closed2)] for pp in [p,-p] \
                    for numStones1 in range(2,6) for numStones2 in range(numStones1,6) \
                        for closed1 in [True,False] for closed2 in [True,False] ]

        if self.featureSet == 2:
            # numStonesNearBy={1: {p:0, -p:0}, 2:{p:0, -p:0}, 3:{p:0, -p:0}}
            # i, j = state[-1]
            # for adj in [1,2,3]:
            #     for ii in range( max(0,i-adj), min(i+adj,self.game.N-1)+1):
            #         for jj in range( max(0,j-adj), min(j+adj,self.game.N-1)+1):
            #             if (ii != i or jj != j) and board[ii][jj] != 0:
            #                 numStonesNearBy[adj][ board[ii][jj] ] += 1
            # feature2 = [ \
            #     numStonesNearBy[1][p],numStonesNearBy[1][-p],\
            #     numStonesNearBy[2][p],numStonesNearBy[2][-p],\
            #     numStonesNearBy[3][p],numStonesNearBy[3][-p],\
            # ]
            return feature1 + feature2

        feature3 = []
        numStonesNearBy={1: {p:0, -p:0}, 2:{p:0, -p:0}, 3:{p:0, -p:0}}
        i, j = state[-1]
        for adj in [1,2,3]:
            for ii in range( max(0,i-adj), min(i+adj,self.game.N-1)+1):
                for jj in range( max(0,j-adj), min(j+adj,self.game.N-1)+1):
                    if (ii != i or jj != j) and board[ii][jj] != 0:
                        numStonesNearBy[adj][ board[ii][jj] ] += 1
        feature3 = [ \
            numStonesNearBy[1][p],numStonesNearBy[1][-p],\
            numStonesNearBy[2][p],numStonesNearBy[2][-p],\
            numStonesNearBy[3][p],numStonesNearBy[3][-p],\
        ]

        if self.featureSet == 3:
            return feature1 + feature2 + feature3

        def outOfBoard(pos):
            return pos[0]<0 or pos[0]>self.game.N-1 or pos[1]<0 or pos[1]>self.game.N-1

        pos = state[-1]
        feature4 = [pos[0],pos[1]]
        for i in range(-self.game.N/4,self.game.N/4+1):
            for j in range(-self.game.N/4,self.game.N/4+1):
                ii, jj = i+pos[0], j+pos[1]
                if outOfBoard( (ii,jj) ):
                    feature4 += [0]
                else:
                    feature4 += [board[i][j]*player]

        if self.featureSet == 4:
            return feature1 + feature2 + feature3 + feature4

        raise ValueError

    def initWeights(self, state): # any non-trivial state
        self.weights = [0]*len(self.getFeature(state,1))

    # def learnWeights(self, trainingSetFile): # linear regression with SGD
    #     examples = []
    #     with open(trainingSetFile,'r') as f:
    #         examples = f.read().split('\n')
    #     for k, example in enumerate(examples): # for each round
    #         endState = tuple([self.game.humanInputToAction(stone) for stone in example.split('-')])
    #         if k==0:
    #             self.initWeights(endState)
    #         for i in range(2,len(endState)):
    #             # def recurse(j):
    #             #     if j == len(endState)+1:
    #             #         return 0
    #             #     state = endState[:j]
    #             #     player = -self.game.player(state)
    #             #     return self.game.reward(state,player) - self.discount*recurse(j+1)
    #             # state = endState[:i+1]
    #             # player = -self.game.player(state)
    #             # y = recurse( i+1 ) # aggreatedScore
    #             state = endState[:i+1]
    #             player = -self.game.player(state)
    #             y = self.game.reward(state,player)
    #             feature = self.getFeature(state,player) # x                
    #             # SGD
    #             loss = dot(self.weights,feature)-y
    #             print 'state, p:', state, player
    #             print '\tloss, y:', loss, y
    #             print '\tw:', self.weights
    #             print '\tfeature:', feature
    #             for j in range(len(self.weights)):
    #                 self.weights[j] -= self.eta*loss*feature[j]
    #             # time.sleep(0.1)

    def learnWeights(self, trainingSetFile): # linear regression with closed form solution (analytical)
        examples = []
        with open(trainingSetFile,'r') as f:
            examples = f.read().split('\n')

        for k, example in enumerate(examples): # for each round
            endState = tuple([self.game.humanInputToAction(stone) for stone in example.split('-')])
            for i in range(1,len(endState)):
                def recurse(j):
                    if j >= len(endState)+1:
                        return 0
                    state = endState[:j]
                    player = -self.game.player(state)
                    # state = endState[:j]
                    # player = -self.game.player(state)
                    # if self.game.isEnd(state):
                    #     return self.game.reward(state,player)

                    return self.game.reward(state,player) - self.discount*recurse(j+1)
                    # return self.game.reward(state,player) + self.discount*recurse(j+2)
                state = endState[:i+1]
                player = -self.game.player(state)
                y = recurse( i+1 ) # aggreatedScore

                # state = endState[:i+1]
                # player = -self.game.player(state)
                # y = self.game.reward(state,player)

                feature = self.getFeature(state,player) # x
                if k==0 and i==1:
                    A = array( feature )
                    Y = [y]
                else:
                    A = vstack( [A, feature] )
                    Y += [y]        
        self.weights = linalg.lstsq(A,Y)[0]
        print self.weights

    def learnWeights_TDLearningFromData(self, trainingSetFile): # linear regression with closed form solution (analytical)
        examples = []
        with open(trainingSetFile,'r') as f:
            examples = f.read().split('\n')

        for k, example in enumerate(examples): # for each round
            endState = tuple([self.game.humanInputToAction(stone) for stone in example.split('-')])
            for i in range(1,len(endState)):                
                def recurse(j):
                    if j >= len(endState)+1:
                        return 0
                    state = endState[:j]
                    player = -self.game.player(state)
                    # state = endState[:j]
                    # player = -self.game.player(state)
                    # if self.game.isEnd(state):
                    #     return self.game.reward(state,player)

                    return self.game.reward(state,player) - self.discount*recurse(j+1)
                    # return self.game.reward(state,player) + self.discount*recurse(j+2)
                state = endState[:i+1]
                player = -self.game.player(state)
                y = recurse( i+1 ) # aggreatedScore

                # state = endState[:i+1]
                # player = -self.game.player(state)
                # y = self.game.reward(state,player)

                feature = self.getFeature(state,player) # x
                if k==0 and i==1:
                    A = array( feature )
                    Y = [y]
                else:
                    A = vstack( [A, feature] )
                    Y += [y]        
        self.weights = linalg.lstsq(A,Y)[0]
        print self.weights

    def loadWeights(self, weightFile):
        with open(weightFile,'r') as f:
            self.weights = [float(w) for w in f.read().split('\n')]

    def saveWeights(self, weightFile):
        with open(weightFile,'w') as f:
            f.write('\n'.join([str(w) for w in self.weights]))

    def evalFunc(self, state, player):
        feature = self.getFeature(state, player)
        return dot(self.weights,feature)

def dot(a, b):
    return sum(i[0]*i[1] for i in zip(a, b))
